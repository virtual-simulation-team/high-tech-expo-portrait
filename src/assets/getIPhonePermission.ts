/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-09-21 15:31:48
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-09-21 15:40:30
 */
/**
 * 获取iPhone陀螺仪权限
 */
export function getIPhonePermission(callBack: () => void) {
  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  window.DeviceOrientationEvent.requestPermission().then(function (
    state: string
  ) {
    // 获取手机陀螺仪
    if (state === "granted") {
      alert("获取权限成功！");
      callBack();
    } else {
      alert("获取权限失败！");
    }
  });
}
