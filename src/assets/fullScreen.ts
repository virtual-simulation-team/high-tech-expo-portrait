/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-09-21 15:35:32
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-09-21 15:35:38
 */
/**
 * 全屏
 */
export function fullScreen() {
  const docElm = document.documentElement; //W3C
  if (docElm.requestFullscreen) {
    docElm.requestFullscreen();
  }
}
