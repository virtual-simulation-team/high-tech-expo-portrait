/*
 * @Descripttion: 
 * @version: 
 * @Author: 刘译蓬
 * @Date: 2022-09-20 15:27:20
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-10-19 11:19:03
 */
import { Color3, HemisphericLight, Scene, Vector3 } from "@babylonjs/core";

export default function skyLight(scene: Scene) {
  const skyLight = new HemisphericLight(
    "HemiLight",
    new Vector3(0, 1, 0),
    scene
  );
  skyLight.diffuse = new Color3(1, 1, 1);
  skyLight.specular = new Color3(1, 1, 1);
  skyLight.groundColor = new Color3(0.5, 0.5, 0.5);
  return skyLight;
}
