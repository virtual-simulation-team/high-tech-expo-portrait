/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-19 15:52:31
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-10-18 10:18:32
 */
import { Scene, SceneLoader } from "@babylonjs/core";
/**
 * @Descripttion: 测试加载
 * @Author: 刘译蓬
 * @msg:
 * @param {Scene} scene
 * @return {*}
 */
const modelLoadTest = async (
  scene: Scene,
  rootUrl: string,
  sceneFilename?: string | File | undefined
) => {
  const container = await SceneLoader.LoadAssetContainerAsync(
    rootUrl,
    sceneFilename,
    scene,
    (progress) => {
      console.log("loading:" + progress.loaded); // 加载进度
    }
  );
  container.addAllToScene(); // 加入场景
};
export default modelLoadTest;
