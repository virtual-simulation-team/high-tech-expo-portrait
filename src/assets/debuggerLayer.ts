/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-09-20 15:27:20
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-10-19 10:23:39
 */
import type { Scene } from "@babylonjs/core";

export default function debuggerLayer(scene: Scene, customKey?: string) {
  window.addEventListener("keydown", (ev) => {
    if (
      ev.shiftKey &&
      ev.ctrlKey &&
      ev.altKey &&
      ev.key === (customKey || "I")
    ) {
      if (scene.debugLayer.isVisible()) {
        scene.debugLayer.hide();
      } else {
        scene.debugLayer.show({ embedMode: true });
      }
    }
  });
}
