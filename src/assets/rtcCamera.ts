/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-09-21 08:53:50
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-09-21 09:46:25
 */
/**
 * 打开摄像头
 */
/**
 * @Descripttion:
 * @Author: 刘译蓬
 * @msg:
 * @param {function} callback
 * @return {*}
 */
export default function rtcCamera(callback: (arg0: HTMLVideoElement) => void) {
  navigator.mediaDevices
    .getUserMedia({
      video: {
        facingMode: "environment",
      },
      // audio: true,
    })
    .then(function (stream: MediaStream) {
      const thisVideo = document.createElement("video");
      thisVideo.srcObject = stream;
      thisVideo.play();
      callback(thisVideo);
    })
    .catch((err: Error) => {
      alert(err.message);
    });
}
