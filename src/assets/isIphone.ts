/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-09-21 15:31:16
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-09-21 15:47:29
 */
/**
 * 判断当前设备是否为iPhone
 * @returns 当前设备是否为iPhone
 */
export function isIPhone() {
  let isIPhone = false;
  if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
    isIPhone = true;
  } else if (/(Android)/i.test(navigator.userAgent)) {
    isIPhone = false;
  }
  return isIPhone;
}
