/*
 * @Descripttion: 
 * @version: 
 * @Author: 刘译蓬
 * @Date: 2022-11-02 14:40:00
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-11-02 15:24:43
 */
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import basicSsl from "@vitejs/plugin-basic-ssl";
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    basicSsl()
  ],
  server:{
    host:true,
    https: true,
    port: 8080,
  }
})
